# mg5env: a mg5amcanlo solution for apple silicon or/and linux
 

contact: Hengne.Li@cern.ch 
date: 8 Feb. 2024


Here we provide some instruction to prepare working area for:

https://indico.ihep.ac.cn/event/7822/page/1116-tool-installation

and or
https://launchpad.net/mg5amcnlo


### Also a docker solution for apple silicon:
https://hub.docker.com/r/lihengne/mg5amcanlo_alma9_arm64


Instructions to configure a working area for MG5 aMC@NLO

Works for CENT OS 7, x86\_64 arch.., and docker with Alma 9.3 base image: 
Also tested for Alma 9.3 or RHEL 9.3 with aarch64 (apple silicon machine with parallels linux)
see the end for specials:


1. install miniconda if you dont have:

```

mkdir -p ~/miniconda3

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh

bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3

~/miniconda3/bin/conda init bash

```

2. untar the codes

```
tar xvzf v3.5.3.tar.gz
```

3. install pkgs in conda

```
conda config --add channels conda-forge 

conda create -n mg5env

conda activate mg4env

conda install gcc=11.4 gfortran=11.4 gxx=11.4 python=3.11 

conda install zlib cmake boost ninja six

```


4. install MG packages

``` 
cd mg5amcnlo-3.5.3/
./bin/mg5\_aMC 
```

this will let you in the MG5 env.

now do the following in the steps:

```
install fastjet --keep_source
```

quit the MG5 env using "quit" command, and check if it installed successfully.

if yes, check if the file exist:
```
ls $PWD/HEPTools/fastjet/bin/fastjet-config
```

if yes, 
put the output of the above command into 
```
./input/mg5\_configuration.txt
```

as 
```
#! fastjet-config
#!  If None: try to find one available on the system
# fastjet = fastjet-config
fastjet = /home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/fastjet/bin/fastjet-config

```

note you should change the beginning part of the location to correctly reflect your case ``/home/lihengne/mg5/mg5env...``


now go inside MG5 again by type 

```
./bin/mg5_aMC
```

then install lhapdf6:
```
install lhapdf6 --keep_source
```

then "quit" again, and open again the file: 

```
./input/mg5\_configuration.txt
```

add those lines similar to below:

```
#! lhapdf-config --can be specify differently depending of your python version
#!  If None: try to find one available on the system
# lhapdf_py2 = lhapdf-config
lhapdf = /home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/lhapdf6_py3/bin/lhapdf-config #
lhapdf_py2 = /home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/lhapdf6_py3/bin/lhapdf-config #
lhapdf_py3 = /home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/lhapdf6_py3/bin/lhapdf-config #
```

make sure to adapt to your env.


go inside MG5 again by typing

```
./bin/mg5_aMC
```

you may find some error messages like this:
```
/home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/lhapdf6_py3/bin/lhapdf-config does not seem to correspond to a valid lhapdf-config executable.
Please set the 'lhapdf' variable to the (absolute) /PATH/TO/lhapdf-config (including lhapdf-config).
Note that you can still compile and run aMC@NLO with the built-in PDFs
 MG5_aMC> set lhapdf /PATH/TO/lhapdf-config

/home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/HEPTools/lhapdf6_py3/bin/lhapdf-config does not seem to correspond to a valid lhapdf-config executable.
Please set the 'lhapdf' variable to the (absolute) /PATH/TO/lhapdf-config (including lhapdf-config).
Note that you can still compile and run aMC@NLO with the built-in PDFs
 MG5_aMC> set lhapdf /PATH/TO/lhapdf-config
```
 
This is because ``HEPTools/lhapdf6_py3/bin/lhapdf-config`` was not properly installed, 

now, "quit" the MG5, and go to the lhapdf6's source directory, into the bin directory, and make install once more:
 
```
cd ./HEPTools/lhapdf6_py3_src/LHAPDF-6.5.3/bin/

make install
```
it should now be installed well.
now you can go back to the working directory, in my case it is:

```
/home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3/
```

and go into MG5 again:

```
./bin/mg5_aMC
```

and install pythia8 and MadAnalysis5 

```
MG5_aMC> install pythia8 --keep_source
MG5_aMC> install MadAnalysis5 --with_fastjet --keep_source 

```


now you can continue your work with MG5.



5. For aarch64 linux, such as apple silicon m3 with RHEL9 in parallels, 
and also for docker Alma or RHEL9 images/containers, do the following:




First, the conda install you should use the arm64 version:

```
https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-aarch64.sh
```


Then, please use v12 of gcc and gfortran, same 3.11 version of python should be ok:
or docker image/container with alma9 aarch64, also need:


```
conda install gcc=12 gxx=12  gfortran=12 python=3.11

conda install make rsync

conda install zlib cmake boost ninja six

``` 

maybe should use yum to install:
 zlib, zlib-devel, make, rsync, cmake




all the other should work well.

When install pythia8, it requires also the old HepMC 2.0.
It installs hepmc 2.0 but it will fail because of old config.guess and config.sub files.

to fix this, please replace the two files using the updated version in the new version in this folder.
e.g.if you are in

```
/home/lihengne/mg5/mg5env/mg5amcnlo-3.5.3-alma9/
```

do :

```
cp ../config.* HEPTools/hepmc_src/hepmc2.06.09/

cd HEPTools/hepmc_src/hepmc2.06.09/

./configure --prefix=/media/psf/Home/local/madgraph/mg5env/mg5amcnlo-3.5.3-alma9/HEPTools/hepmc/ --with-momentum=GEV --with-length=MM 

make

rm ../../hepmc/lib

make install

```

please  make sure in the configure step above, the prefix should be set to the location in your working area.


Then, verify the ``input/mg5\_configuration.txt'' to see if you have the hepmc_path setup properly.

```
#! Herwig++/Herwig7 paths
#!  specify here the paths also to HepMC ant ThePEG
#!  define the path to the herwig++, thepeg and hepmc directories.
#!  paths can be absolute or relative from mg5 directory
#!  WARNING: if Herwig7 has been installed with the bootstrap script,
#!  then please set thepeg_path and hepmc_path to the same value as
#!  hwpp_path
# hwpp_path =
# thepeg_path = 
# hepmc_path =
hepmc_path = /media/psf/Home/local/madgraph/mg5env/mg5amcnlo-3.5.3-alma9/HEPTools/hepmc
```

then  go back to 

and go into MG5 again:

```
./bin/mg5_aMC
```

and re-do the installation of pythia8, it should work well:

```
MG5_aMC> install pythia8 --keep_source

```

MadAnalysis5 should work well with aarch64 linux.
```
install MadAnalysis5 --with_fastjet --keep_source
```
or
```
install MadAnalysis5 --with_fastjet --keep_source --no_MA5_further_install --no_root_in_MA5
 
```





